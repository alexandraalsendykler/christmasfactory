﻿namespace Entities
{
    public class Elf
    {
        #region ATTRIBUTES
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsAvailable { get; set; }
        public string Picture { get; set; }
        #endregion ATTRIBUTES

        #region CONSTRUCTOR

        public Elf()
        {

        }
        public Elf(int id, string name, bool isAvailable, string picture)
        {
            Id = id;
            Name = name;
            IsAvailable = isAvailable;
            Picture = picture;
        }
        #endregion CONSTRUCTORS

    }
}