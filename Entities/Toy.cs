﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class Toy
    {
        #region ATTRIBUTES
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public float Price { get; set; }
        public int? IdResponsable { get; set; }
        #endregion ATTRIBUTES

        #region CONSTRUCTOR
        public Toy()
        {

        }

        public Toy(int id, string name, string description, float price, int idResponsable)
        {
            Id = id;
            Name = name;
            Description = description;
            Price = price;
            IdResponsable = idResponsable;

        }
        #endregion CONSTRUCTORS
    }
}
