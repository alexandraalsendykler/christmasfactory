﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MySql.Data.MySqlClient;
using System.Data.Common;

using Fr.EQL.AI110.ChristmasFactory.Entities;

namespace DataAccess
{
    public class ElfDAO
    {
        public void Insert(Elf elf)
        {
            // créer un objet connection :
            DbConnection cnx = new MySqlConnection();

            // 1 - Paramétrer la connection :
            cnx.ConnectionString = "Server=localhost;Database=christmas_bdd;Uid=root;Pwd=root;";

            // 2 - Paramétrer la commande :
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"INSERT INTO elf 
                                (name, is_available, picture) 
                                VALUES 
                                (@name, @isAvailable, @picture)";

            cmd.Parameters.Add(new MySqlParameter("name", elf.Name));
            cmd.Parameters.Add(new MySqlParameter("isAvailable", elf.IsAvailable));
            cmd.Parameters.Add(new MySqlParameter("picture", elf.Picture));

            // 3 - ouvrir la connection :
            cnx.Open();

            // 4 - Exécuter la commande :
            int nbLignes = cmd.ExecuteNonQuery();

            // 5 - Traiter le résultat :
            Console.WriteLine(nbLignes + " ligne insérée");

            // 6 - fermer la conneciton :
            cnx.Close();
        }

        public void Update(Elf elf)
        {

        }

        public void Delete(int id)
        {

        }

        public Elf GetById(int id)
        {
            return null;
        }

        public List<Elf> GetAll()
        {
            //             java vs    C#
            // interface : List       IList 
            // classe :    ArrayList  List

            return null;
        }
    }
}
